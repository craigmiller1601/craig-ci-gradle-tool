# craig-ci-gradle-tool

Used to analyze Gradle projects for the `craig-ci` pipeline and output important project information.

## How to Run to Test

```bash
gradle run --args="/Users/craigmiller/Development/Libraries/craig-defaults-gradle-plugin"
```

## Building

Because this is a critical part of `craig-ci`, it cannot be built through the CI/CD pipeline. Instead, run the `build.sh` script to build & publish the artifact. 