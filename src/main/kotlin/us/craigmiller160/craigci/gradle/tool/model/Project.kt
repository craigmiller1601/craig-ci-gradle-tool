package us.craigmiller160.craigci.gradle.tool.model

data class Project(val info: Item, val dependencies: List<Item>)
