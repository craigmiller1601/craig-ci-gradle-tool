package us.craigmiller160.craigci.gradle.tool.error

class UnresolvedDependencyException(msg: String) :
    RuntimeException("Unable to resolve dependency: $msg")
