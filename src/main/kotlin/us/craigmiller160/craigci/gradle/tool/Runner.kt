package us.craigmiller160.craigci.gradle.tool

import com.fasterxml.jackson.databind.ObjectMapper
import io.craigmiller160.craigbuild.gradle.plugin.model.CraigBuildProject
import java.io.File
import org.gradle.tooling.GradleConnector
import org.gradle.tooling.model.ExternalDependency
import org.gradle.tooling.model.idea.IdeaProject
import us.craigmiller160.craigci.gradle.tool.model.Item
import us.craigmiller160.craigci.gradle.tool.model.Project

fun main(args: Array<String>) {
  if (args.isEmpty()) {
    throw RuntimeException("Must provide project file path")
  }

  val gradleHomeDirectory =
      System.getenv("GRADLE_INSTALL_HOME")
          ?: throw RuntimeException("Must provide GRADLE_INSTALL_HOME")
  val (craigBuildProject, ideaProject) =
      GradleConnector.newConnector()
          .useInstallation(File(gradleHomeDirectory))
          .forProjectDirectory(File(args.first()))
          .connect()
          .use { connection ->
            Pair(
                connection.getModel(CraigBuildProject::class.java),
                connection.getModel(IdeaProject::class.java))
          }
  val project =
      Project(
          info =
              Item(
                  group = craigBuildProject.group,
                  name = craigBuildProject.name,
                  version = craigBuildProject.version),
          dependencies = getDependencies(ideaProject))

  val json = ObjectMapper().writeValueAsString(project)
  println(json)
}

private fun getDependencies(model: IdeaProject): List<Item> =
    model.modules.all
        .asSequence()
        .flatMap { it.dependencies.all.asSequence() }
        .filter { it is ExternalDependency }
        .map { it as ExternalDependency }
        .map { dependency -> dependency.gradleModuleVersion }
        .filterNotNull()
        .map { Item(group = it.group, name = it.name, version = it.version) }
        .toList()
