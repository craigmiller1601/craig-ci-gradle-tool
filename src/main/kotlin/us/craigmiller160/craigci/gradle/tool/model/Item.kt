package us.craigmiller160.craigci.gradle.tool.model

data class Item(val group: String, val name: String, val version: String)
